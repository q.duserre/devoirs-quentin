<!DOCTYPE html>
<html>

<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body>
<form method="post" action="main.php">

<p>
<h1>Sondage calcul IMC / tranche d'âge & Régime</h1>
</p>
<a href="main.php">login</a>
<div class="form-group">
<a>Date de naissance</a><input type="date" class="form-control" name="date" /><br>

<a>Régime</a><br><br>
<input type="radio" name="regime" value="Omnivore" id="oui" /> <label for="oui">Omnivore</label>
<input type="radio" name="regime" value="Végétarien" id="oui" /> <label for="oui">Végétarien</label>
<input type="radio" name="regime" value="Vegan" id="oui" /> <label for="oui">Vegan</label><br>
<a href=''><a>
<label for="taille">Taille(<p style="display:inline;"id="width"></p>cm)</label>
<input type="range"oninput="document.getElementById('width').
innerText=this.value"onchange="document.-getElementById('width').
innerText=this.value;"min="100"max="200"name="taille"id="taille"class="form-control"required/><br/>

<label for="poids">Poids(<p style="display:inline;"id="weight"></p>kg)</label>
<input type="range"oninput="document.getElementById('weight').
innerText=this.value"onchange="document.getElementById('weight').
innerText=this.value;"min="40"max="180"name="poids"id="poids"class="form-control"required/><br/>

<a>Login</a><input type="text" class="form-control" name="login" /><br>
<a>Password</a><input type="password" class="form-control" name="mdp" /><br>

</form>

<p>
  <a class="btn btn-danger" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
    Collecte de données
  </a>
</p>
<div class="collapse" id="collapseExample">
  <div class="card card-body">
Les informations recueillies sur ce formulaire sont enregistrées dans un fichier informatisé par [identité et coordonnées du responsable de traitement] pour [finalités du traitement]. La base légale du traitement est [base légale du traitement].
Les données collectées seront communiquées aux seuls destinataires suivants : [destinataires des données].
Les données sont conservées pendant [durée de conservation des données prévue par le responsable du traitement ou critères permettant de la déterminer].
Vous pouvez accéder aux données vous concernant, les rectifier, demander leur effacement ou exercer votre droit à la limitation du traitement de vos données. (en fonction de la base légale du traitement, mentionner également : Vous pouvez retirer à tout moment votre consentement au traitement de vos données ; Vous pouvez également vous opposer au traitement de vos données ; Vous pouvez également exercer votre droit à la portabilité de vos données)
Consultez le site cnil.fr pour plus d’informations sur vos droits.
Pour exercer ces droits ou pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter (le cas échéant, notre délégué à la protection des données ou le service chargé de l’exercice de ces droits) : [adresse électronique, postale, coordonnées téléphoniques, etc.]
Si vous estimez, après nous avoir contactés, que vos droits « Informatique et Libertés » ne sont pas respectés, vous pouvez adresser une réclamation à la CNIL.
  </div>
</div>
<input type="checkbox" name="données" value="oui" id="oui" /> <label for="oui">J'accepte la collecte des données</label><br>
<input type="submit" value="Envoyer" />

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
